//
//  Observable.swift
//  wallet-hub
//
//  Created by Victor Alves De Freitas on 01/05/19.
//  Copyright © 2019 brvlab. All rights reserved.
//

import Foundation

class Observable<T> {
    var value: T {
        didSet {
            DispatchQueue.main.async {
                self.valueChanged?(self.value)
            }
        }
    }
    
    init(_ v: T) {
        value = v
    }
    
    var valueChanged: ((T) -> Void)?
}
