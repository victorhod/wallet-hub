//
//  WalletModel.swift
//  wallet-hub
//
//  Created by Victor Alves De Freitas on 01/05/19.
//  Copyright © 2019 brvlab. All rights reserved.
//

import Foundation

struct AccountCard: Codable {
    var walletId: String?
    var cardType: CardType?
    var cardStatus: CardStatus?
    var lastNumber: String?
}

struct PrePaidCard: Codable {
    var walletId: String?
    var cardType: CardType?
    var cardStatus: CardStatus?
    var lastNumber: String?
    var estimatedDeliveryDate: String?
}

struct CreditCard: Codable {
    var walletId: String?
    var cardType: CardType?
    var cardStatus: CardStatus?
    var lastNumber: String?
}

enum CardStatus: String, Codable {
    case ACTIVE = "ACTIVE"
    case IN_PRODUCTION = "IN_PRODUCTION"
}

enum CardType: String, Codable {
    case ACCOUNT = "ACCOUNT"
    case CREDIT = "CREDIT"
    case PREPAID = "PRE-PAID"
}
