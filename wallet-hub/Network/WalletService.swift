//
//  WalletService.swift
//  wallet-hub
//
//  Created by Victor Alves De Freitas on 01/05/19.
//  Copyright © 2019 brvlab. All rights reserved.
//

import Foundation

class WalletService: NSObject {
    
    var base = "http://private-89d2b5-pscards.apiary-mock.com"
    
    // MARK: Services
    
    func fetchAccountCards(completion: @escaping ((Result<[AccountCard], Error>) -> Void)) {
        let url = URL(string: base + "/account/cards")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let error = error else {
                if let data = data {
                    do {
                        let accountCards = try JSONDecoder().decode([AccountCard].self, from: data)
                        completion(.success(accountCards))
                    } catch let e {
                        completion(.failure(e))
                    }
                }
                return
            }
            
            completion(.failure(error))
        }.resume()
    }
    
    func fetchPrePaidCards(completion: @escaping ((Result<[PrePaidCard], Error>) -> Void)) {
        let url = URL(string: base + "/pre-paid/cards")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let error = error else {
                if let data = data {
                    do {
                        let accountCards = try JSONDecoder().decode([PrePaidCard].self, from: data)
                        completion(.success(accountCards))
                    } catch let e {
                        completion(.failure(e))
                    }
                }
                return
            }
            
            completion(.failure(error))
        }.resume()
    }
    
    func fetchCreditCards(completion: @escaping ((Result<[CreditCard], Error>) -> Void)) {
        let url = URL(string: base + "/credit/cards")!
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let error = error else {
                if let data = data {
                    do {
                        let accountCards = try JSONDecoder().decode([CreditCard].self, from: data)
                        completion(.success(accountCards))
                    } catch let e {
                        completion(.failure(e))
                    }
                }
                return
            }
            
            completion(.failure(error))
        }.resume()
    }
}
