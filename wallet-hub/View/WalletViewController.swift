//
//  ViewController.swift
//  wallet-hub
//
//  Created by Victor Alves De Freitas on 01/05/19.
//  Copyright © 2019 brvlab. All rights reserved.
//

import UIKit

class WalletViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var cardsDispatchGroup = DispatchGroup()
    var service = WalletService()
    var cards: [Any] = [Any]()
    var psCards: [(cards: [Any], order: Int)] = [(cards: [], order: 0)]
    var refreshControl = UIRefreshControl()
    var skeletonCells = [Int]()
    var isLoadingStatus = [Bool]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
        DispatchQueue.global().async {
            self.fetchCards()
        }
    }
    
    func setupTableView() {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: "CardTableViewCell", bundle: nil), forCellReuseIdentifier: "CardTableViewCell")
        
        refreshControl.addTarget(self, action: #selector(fetchCards), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }
    
    private func resetArrays() {
        cards.removeAll()
        psCards.removeAll()
        skeletonCells.removeAll()
    }
    
    @objc func fetchCards() {
        resetArrays()
        skeletonCells.append(contentsOf: [1, 2, 3])
        isLoadingStatus.append(contentsOf: [true, true, true])
        cardsDispatchGroup.enter()
        service.fetchAccountCards { [weak self] result in
            switch result {
            case .success(let accountCards):
                DispatchQueue.main.asyncAfter(deadline: .now() + 6, execute: {
                    self?.psCards.append((cards: accountCards, order: 1))
                    self?.isLoadingStatus[0] = false
                    self?.reloadTable()
                    self?.cardsDispatchGroup.leave()
                })
            case .failure(let error):
                self?.cardsDispatchGroup.leave()
                debugPrint(error)
            }
        }
        
        cardsDispatchGroup.enter()
        service.fetchCreditCards { [weak self] result in
            switch result {
            case .success(let creditCards):
                DispatchQueue.main.asyncAfter(deadline: .now() + 4, execute: {
                    self?.psCards.append((cards: creditCards, order: 2))
                    self?.isLoadingStatus[1] = false
                    self?.reloadTable()
                    self?.cardsDispatchGroup.leave()
                })
            case .failure(let error):
                self?.cardsDispatchGroup.leave()
                debugPrint(error)
            }
        }
        
        cardsDispatchGroup.enter()
        service.fetchPrePaidCards { [weak self] result in
            switch result {
            case .success(let prePaidCards):
                DispatchQueue.main.asyncAfter(deadline: .now() + 2, execute: {
                    self?.psCards.append((cards: prePaidCards, order: 3))
                    self?.isLoadingStatus[2] = false
                    self?.reloadTable()
                    self?.cardsDispatchGroup.leave()
                })
            case .failure(let error):
                self?.cardsDispatchGroup.leave()
                debugPrint(error)
            }
        }
    }
    
    private func reloadTable() {
        DispatchQueue.main.async {
            self.cards.removeAll()
            _ = self.skeletonCells.popLast()
            
            self.psCards.sort { $0.order < $1.order }
            self.psCards.forEach({ (tuple) in
                self.cards.append(contentsOf: tuple.cards)
            })
            
            self.tableView.reloadData()
            self.refreshControl.endRefreshing()
        }
    }
}

extension WalletViewController: UITableViewDelegate, UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cards.count + skeletonCells.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CardTableViewCell", for: indexPath) as? CardTableViewCell
        cell?.isLoading.value = true
        if cards.count > 0  {
            if indexPath.row < cards.count {
                cell?.setup(cards[indexPath.row])
                updateLoadingState(cell)
            }
        }
        
        return cell!
    }
    
    func updateLoadingState(_ cell: CardTableViewCell?) {
        cell?.isLoading.value = false
    }
}

