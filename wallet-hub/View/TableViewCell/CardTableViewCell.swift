//
//  CardTableViewCell.swift
//  wallet-hub
//
//  Created by Victor Alves De Freitas on 01/05/19.
//  Copyright © 2019 brvlab. All rights reserved.
//

import UIKit
import SkeletonView

class CardTableViewCell: UITableViewCell {

    @IBOutlet weak var cardView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lastNumbersLabel: UILabel!
    
    var isLoading: Observable<Bool> = Observable(false)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        registerObservable()
    }
    
    func registerObservable() {
        isLoading.valueChanged = { [weak self] status in
            if status {
                self?.showGradientSkeleton()
                self?.startSkeletonAnimation()
            } else {
                self?.hideSkeleton()
                self?.stopSkeletonAnimation()
            }
        }
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
    }
    
    func setup<T>(_ model: T) {
        cardView.layer.cornerRadius = 5
        cardView.layer.masksToBounds = true
        
        if let accountCard =  model as? AccountCard {
            cardView.backgroundColor = .silver
            nameLabel.text = "Conta"
            lastNumbersLabel.text = accountCard.lastNumber
        } else if let creditCard = model as? CreditCard {
            cardView.backgroundColor = .gray
            nameLabel.text = "Credito"
            lastNumbersLabel.text = creditCard.lastNumber
        } else if let prePaidCard = model as? PrePaidCard {
            cardView.backgroundColor = .orange
            nameLabel.text = "Pre pago"
            lastNumbersLabel.text = prePaidCard.lastNumber
        } else {
            debugPrint("Card is not valid")
        }
    }
}
